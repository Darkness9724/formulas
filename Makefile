LATEXMK = latexmk -synctex=1 -lualatex -shell-escape
QPDF = qpdf --object-streams=generate --compress-streams=y \
	--stream-data=compress --compression-level=9 \
	--recompress-flate --optimize-images --linearize \
	--replace-input
TARGET = formulas

default: build optimaze
.PHONY: clean

build: $(TARGET).tex
	$(LATEXMK) $<

optimaze: $(TARGET).pdf
	$(QPDF) $<

clean:
	latexmk -c
