\section{Электричество}

\subsection{Электростатика}

Закон сохранения заряда в замкнутой системы:
\begin{equation}
    \Sum[i]{q_i} = const
\end{equation}
\(q\) --- заряд.

Закон Кулона:
\begin{equation}
    F = \frac{1}{4\pi\epsilon_0}\frac{|q_1||q_2|}{\epsilon r^2}
\end{equation}
\(F\) --- сила взаимодействия двух точечных зарядов \(q_1\) и \(q_2\);
\(r\) --- расстояние между ними;
\(\epsilon_0\) --- электрическая постоянная;
\(\epsilon\) --- диэлектрическая проницаемость среды (для ваккума \(\epsilon = 1\)).

Напряжённость электростатического поля:
\begin{equation}
    \vec{E} = \frac{\vec{F}}{q_+}
\end{equation}
\(F\) --- сила, действующая на точечный положительный заряд \(q_+\), помещенный в данную точку поля.

Напряжённость электростатического поля точечного заряда \(q\) на расстоянии \(r\) от заряда:
\begin{equation}
    E = \frac{1}{4\pi\epsilon_0}\frac{q}{r^2}
\end{equation}

Поток вектора напряжённости электростатического поля:
\begin{align}
    &\dd{\Phi_E} = \vec{E} \dd{\vec{S}} = E_n \dd{S}
    &\qquad \text{(сквозь площадку dS)}
    &\\
    &\dd{\Phi_E} = \integral[S]{\vec{E}}{\vec{S}} = \integral[S]{E_n}{S}
    &\qquad \text{(сквозь поверхность S)}
    &\\
    &\dd{\Phi_E} = \ointegral[S]{\vec{E}}{\vec{S}} = \ointegral[S]{E_n}{S}
    &\qquad \text{(сквозь замкнутую поверхность S)}
\end{align}

Принцип суперпозиции электростатических полей:
\begin{equation}
    \vec{E} = \Sum[i=1][n]{\vec{E_i}}
\end{equation}
\(E_i\) --- напряжённость поля, создаваемого зарядом \(q_i\).

Теорема Гаусса для электростатического поля в ваккуме в случае дискретного распределения зарядов:
\begin{equation}
    \ointegral[S]{\vec{E}}{\vec{S}} =\ointegral[S]{E_n}{S} = \frac{1}{\epsilon_0}\Sum[i=1][n]{\vec{q_i}}
\end{equation}

Она же в случае непрерывного распределения зарядов:
\begin{equation}
    \ointegral[S]{\vec{E}}{\vec{S}} =\ointegral[S]{E_n}{S} = \frac{1}{\epsilon_0} \integral[V]{\rho}{V}
\end{equation}

Напряжённость поля, создаваемого равномерно заряжённой бесконечной плоскостью:
\begin{equation}
    E_{\infty} = \frac{\sigma}{2\epsilon_0}
\end{equation}
\(\sigma\) --- поверхностная плотность заряда.

Напряжённость поля, создаваемого двумя бесконечными параллельными равномерно заряжёнными плоскостями:
\begin{equation}
    E_{\infty} = \frac{\sigma}{\epsilon_0}
\end{equation}

Напряжённость поля, создаваемого равномерно заряжённой сферической поверхностью радиусом \(R\) с общим зарядом \(q\) на расстоянии \(r\) от центра:
\begin{align}
    &E = 0
    &\qquad \text{при \(r < R\) (внутри сферы)}
    &\\
    &E = \frac{1}{4\pi\epsilon_0}\frac{q}{r^2}
    &\qquad \text{при \(r \geqslant R\) (вне сферы)}
\end{align}

Напряжённость поля, создаваемого объёмно заряжённым шаром радиусом \(R\) с общим зарядом \(q\) на расстоянии \(r\) от центра шара:
\begin{align}
    &E = \frac{1}{4\pi\epsilon_0}\frac{q}{R^3}r
    &\qquad \text{при \(r < R\) (внутри шара)}
    &\\
    &E = \frac{1}{4\pi\epsilon_0}\frac{q}{r^2}
    &\qquad \text{при \(r \geqslant R\) (вне шара)}
\end{align}

Напряжённость поля, создаваемого равномерно заряжённым бесконечным цилиндром радиусом \(R\) с общим зарядом \(q\) на расстоянии \(r\) от оси цилиндра:
\begin{align}
    &E = 0
    &\qquad \text{при \(r < R\) (внутри цилиндра)}
    &\\
    &E = \frac{1}{2\pi\epsilon_0}\frac{\tau}{r}
    &\qquad \text{при \(r \geqslant R\) (вне цилиндра)}
\end{align}
\(\tau\) --- линейная плотность заряда.

Циркуляция вектора напряжённости электростатического поля вдоль замкнутого контура:
\begin{equation}
    \ointegral[L]{\vec{E}}{\vec{\ell}} = \ointegral[L]{E_{\ell}}{\ell} = 0
\end{equation}
\(E_{\ell}\) --- проекция вектора \(\vec{E}\) на направление элементарного перемещения \(\dd\vec{\ell}\).

Потенциальная энергия заряда \(q_0\) в поле заряда \(q\) на расстоянии \(r\) от него:
\begin{equation}
    \Pi = \frac{1}{4\pi\epsilon_0}\frac{q q_0}{r}
\end{equation}

Потенциал электростатического поля:
\begin{equation}
    \varphi = \frac{\Pi}{q_0} = \frac{A_{\infty}}{q_0}
\end{equation}
\(q_0\) --- точечный положительный заряд, помещённый в данную точку поля;
\(\Pi\) --- потенциальная энергия заряда \(q_0\);
\(A_{\infty}\) --- работа перемещения заряда \(q_0\) из данной точки поля за его пределы.

Потенциал электростатического поля точечного заряда \(q\) на расстоянии \(r\):
\begin{equation}
    \varphi = \frac{1}{4\pi\epsilon_0}\frac{q}{r}
\end{equation}

Связь между напряжённостью и потенциалом электростатического поля:
\begin{equation}
    \vec{E} = -\nabla\varphi
\end{equation}

В случае поля, обладающего центральной или осевой симметрией:
\begin{equation}
    E = -\frac{\dd\varphi}{\dd{r}}
\end{equation}

Работа, совершаемая силами электростатического поля при перемещении заряда \(q_0\) из точки 1 в точку 2:
\begin{equation}
    A = q_0 (\varphi_1 - \varphi_2) = q_0 \integral[1][2]{\vec{E}}{\vec{\ell}} = q_0 \integral[1][2]{E_l}{\ell}
\end{equation}

Поляризованность диэлектрика:
\begin{equation}
    \vec{P} = \frac{\Sum[i]{\vec{B}_i}}{V}
\end{equation}
\(B\) --- дипольный момент \(i\)-й молекулы;
\(V\) --- объём диэлектрика.

Связь между поляризованностью диэлектрика и напряжённостью электростатического поля:
\begin{equation}
    \vec{P} = {\ae}\epsilon_0\vec{E}
\end{equation}
\({\ae}\) --- диэлектрическая восприимчивость вещества.

Связь между диэлектрической проницаемостью \(e\) и диэлектрической восприимчивостью \({\ae}\):
\begin{equation}
    \epsilon = 1 + {\ae}
\end{equation}

Связь между векторами электростатического смещения \(\vec{D}\), напряжённости электростатического поля \(\vec{E}\) и Поляризованности \(\vec{P}\):
\begin{equation}
    \vec{D} = \epsilon\vec{E}+\vec{P} = \epsilon_0 \epsilon\vec{E}
\end{equation}

Теорема Гаусса для электростатического поля в диэлектрике:
\begin{equation}
    \ointegral[S]{\vec{D}}{\vec{S}} = \ointegral[S]{D_n}{S} = \Sum[i=1][n]{q_i}
\end{equation}

Условия на границе раздела диэлектрических сред, проницаемость которых \(\epsilon_1\) и \(\epsilon_2\), при отсутствии на границе свободных зарядов:
\begin{align}
    &E_{1\tau} = E_{2\tau}
    &\qquad D_{1n} = D_{2n}
    &\qquad \frac{D_{1n}}{D_{2n}} = \frac{\epsilon_1}{\epsilon_2}
    &\qquad \frac{E_{1\tau}}{E_{2\tau}} = \frac{\epsilon_2}{\epsilon_1}
\end{align}

Напряжённость электростатического поля у поверхности проводника:
\begin{equation}
    E = \frac{\sigma}{\epsilon_0 \epsilon}
\end{equation}

Электроёмкость уединённого проводника:
\begin{equation}
    C = \frac{q}{\varphi}
\end{equation}

Электроёмкость шара радиусом \(R\):
\begin{equation}
    C = 4\pi\epsilon_0 \epsilon R
\end{equation}

Электроёмкость конденсатора:
\begin{equation}
    C = \frac{q}{\varphi_1 - \varphi_2}
\end{equation}
q --- заряд, накопленный в конденсаторе.

Электроёмкость плоского конденсатора:
\begin{equation}
    C = \frac{\epsilon_0 \epsilon S}{r}
\end{equation}
\(S\) --- площадь каждой пластины конденсатора;
\(r\) --- расстояние между пластинами.

Электроёмкость сферического конденсатора:
\begin{equation}
    C = 4\pi\epsilon_0\epsilon\frac{r_1 r_2}{r_2 - r_1}
\end{equation}
\(r_1\) и \(r_2\) --- радиусы концентрических сфер.

Энергия уединённого заряжённого проводника:
\begin{equation}
    W = \frac{C\varphi^2}{2} = \frac{q\varphi}{2} = \frac{q^2}{2C}
\end{equation}

Сила притяжения между двумя равномерно заряжёнными обкладками конденсатора:
\begin{equation}
    |F| = \frac{q^2}{2\epsilon_0\epsilon S} = \frac{\sigma^2 S}{2\epsilon_0 \epsilon} = \frac{\epsilon_0 \epsilon E^2 S}{2}
\end{equation}

Энергия электростатического поля плоского контура:
\begin{equation}
    W = \frac{\epsilon_0 \epsilon E^2}{2}V = \frac{\epsilon_0\epsilon S \Pi^2}{2r}
\end{equation}
\(S\) --- площадь одной пластины;
\(\Pi\) --- разность потенциалов между пластинами;
\(V\) --- объём конденсатора.

Объёмная плотность энергии электростатического поля:
\begin{equation}
    w = \frac{\epsilon_0 \epsilon E^2}{2} = \frac{E D}{2}
\end{equation}

Сила тока:
\begin{equation}
    I = \frac{\dd{q}}{t}
\end{equation}
\(q\) --- количество заряда, прошедшего за промежуток времени \(t\).

Плотность тока в проводнике
\begin{align}
    j = \frac{I}{S}
    \qquad \vecj = ne\langle\vec{v}\rangle
\end{align}
\(S\) --- площадь поперечного сечения проводника;
\(n\) --- концентрация зарядов;
\(\langle\vec{v}\rangle\) --- средняя скорость упорядоченного движения зарядов в проводнике.

Электродвижущая сила в цепи:
\begin{align}
    &\Emf = \frac{A_{ст}}{q_0}
    &\\
    &\Emf = \ointegral{\vec{E}_{ст}}{\vec{\ell}}
    &\qquad \text{(для замкнутой цепи)}
    &\\
    &\Emf = \ointegral[1][2]{\vec{E}_{ст}}{\vec{\ell}}
    &\qquad \text{(для участка цепи 1 и 2)}
\end{align}
\(A_{ст}\) --- работа сторонних сил;
\(q_0\) --- единичный положительный заряд;
\(E_{ст}\) --- напряжённость поля сторонних сил.

Разность потенциалов между двумя точками цепи:
\begin{equation}
    \varphi_1 - \varphi_2 = \integral[1][2]{\vec{E}}{\vec{\ell}} = \integral[1][2]{E_{\ell}}{\ell}
\end{equation}

Напряжение на участке цепи:
\begin{equation}
    U = \varphi_1 - \varphi_2 + \Emf
\end{equation}

Сопротивление однородного линейного проводника и его удельная проводимость:
\begin{align}
    R = \rho\frac{\ell}{S}
    \qquad \gamma = \frac{1}{\rho}
\end{align}
\(\rho\) --- удельное электрическое сопротивление;
\(S\) --- площадь поперечного сечения.

Закон Ома о силе тока:
\begin{align}
    &I = \frac{U}{R}
    &\qquad \text{(для однородного участка цепи)}
    &\\
    &I = \frac{\varphi_1 - \varphi_2 + \Emf_{12}}{R}
    &\qquad \text{(для неоднородного участка цепи)}
    &\\
    &I = \frac{\Emf}{R}
    &\qquad \text{(для замкнутой цепи)}
\end{align}
\(U\) --- напряжение на участке цепи;
\(R\) --- сопротивление цепи (или его участка);
\((\varphi_1 - \varphi_2)\) --- разность потенциала на концах участка цепи;
\(\Emf_{12}\) --- электродвижущая сила источников тока, входящих в участок;
\(\Emf\) --- электродвижущая сила всех источников тока в цепи.

Зависимость удельного сопротивления \(\rho\) и сопротивления \(R\) от температуры:
\begin{align}
    \rho = \rho_0 (1+\alpha t)
    \qquad R = R_0 (1+\alpha t)
\end{align}

Закон Ома в дифференциальной форме:
\begin{equation}
    \vecj = \gamma{\vec{E}}
\end{equation}

Работа тока:
\begin{equation}
    \dd{A} = U \dd{q} = I U \dd{t} = I^2 R \dd{t} = \frac{U^2}{R} \dd{t}
\end{equation}

Мощность тока:
\begin{equation}
    P = \frac{\dd{A}}{\dd{t}} = U I = I^2 R = \frac{U^2}{R}
\end{equation}

Закон Джоуля-Ленца:
\begin{equation}
    \dd{q} = I U \dd{t} = I^2 R \dd{t}
\end{equation}

Закон Джоуля-Ленца в дифференциальной форме:
\begin{equation}
    w = j E = \gamma E^2
\end{equation}
\(w\) --- удельная тепловая мощность тока;
\(j\) --- плотность тока;
\(E\) --- напряжённость электростатического поля;
\(\gamma\) --- удельная электрическая проводимость вещества.

Правило Кирхгрофа:
\begin{align}
    \Sum[k]{I_k} = 0
    \qquad \Sum[i]{I_i R_i} = \Sum[k]{\Emf_k}
\end{align}

\subsection{Электродинамика}

Механический момент, действующий на контур с током, помещённый в однородное магнитное поле:
\begin{equation}
    \vec{M} = [\vec{m}\vec{B}]
\end{equation}

Модуль механического момента:
\begin{equation}
    M = m B\sin{\alpha}
\end{equation}
\(\alpha\) --- угол между нормалью к плоскости контура и вектором \(\vec{B}\).

Магнитный момент контура с током:
\begin{equation}
    \vec{m} = IS\vec{n}
\end{equation}
\(S\) --- площадь контура с током;
\(\vec{n}\) --- единичный вектор нормали к поверхности контура.

Связь между магнитной индукцией \(\vec{B}\) и напряжённостью \(\vec{H}\) магнитного поля:
\begin{equation}
    \vec{B} = \mu_0 \mu \vec{H}
\end{equation}
\(\mu_0\) --- магнитная постоянная;
\(mu\) --- магнитная проницаемость среды.

Закон Био-Савара-Лапласа (магнитная индукция поля \(\dd\vec{B}\), создаваемая элементом длины \(\dd\vec{\ell}\) проводника с силой тока \(I\)):
\begin{equation}
    \dd\vec{B} = \frac{\mu_0 \mu}{4\pi} \frac{I[\dd\vec{\ell}, \vec{r}]}{r^3}
\end{equation}
\(\vec{r}\) --- радиус-вектор, проведённый от \(\dd\vec{\ell}\) к точке, в которой определяется магнитная индукция.

\begin{figure}
    \centering
    \caption{Правило буравчика, оно же правило правой руки}
    \includesvg{right_hand_rule}
\end{figure}

Модуль вектора \(\dd\vec{B}\):
\begin{equation}
    \dd{B} = \frac{\mu_0 \mu}{\mu}\frac{I\dd\ell\sin{\alpha}}{r^2}
\end{equation}

Принцип суперпозиции магнитных полей:
\begin{equation}
    \vec{B} = \Sum[i]{\vec{B}_i}
\end{equation}

Магнитная индукция, создаваемая бесконечно длинным прямым проводником по которому идёт ток:
\begin{equation}
    B = \frac{\mu_0 \mu}{4\pi}\frac{2\ell}{R}
\end{equation}
\(R\) --- расстояние от оси проводника.

Магнитная индукция в центре кругового проводника с током:
\begin{equation}
    B = \mu_0 \mu \frac{I}{2R}
\end{equation}
\(R\) --- радиус проводника.

Закон Ампера (сила \(\dd\vec{F}\), действующая на элемент длины \(\dd\vec{\ell}\) проводника с током \(I\), помещённый в магнитное поле с индукцией \(\vec{B}\)):
\begin{equation}
    \dd\vec{F} = I[\dd\vec{\ell}, \vec{B}]
\end{equation}

Модуль вектора \(\dd\vec{F}\):
\begin{equation}
    \dd{F} = IB \dd\ell\sin{\alpha}
\end{equation}

Сила взаимодействия двух бесконечных прямолинейных параллельных проводников с токами \(I_1\) и \(I_2\):
\begin{equation}
    \dd{F} = \frac{\mu_0 \mu}{4\pi}\frac{2I_1 I_2}{r}\dd\ell
\end{equation}
\(r\) --- расстояние между проводниками;
\(\dd\ell\) --- отрезок проводника.

Магнитная индукция поля точечного заряда \(q\), свободно движущегося с нерелятивистской скоростью \(\vec{v}\):
\begin{equation}
    \vec{B} = \frac{\mu_0 \mu}{4\pi}\frac{q[\vec{v}\vec{r}]}{r^3}
\end{equation}

Модуль вектора \(\vec{B}\):
\begin{equation}
    B = \frac{\mu_0 \mu}{4\pi}\frac{qv}{r^2}\sin{\alpha}
\end{equation}

Сила Лоренца (сила, действующая на заряд \(q\), движущийся в магнитном поле \(\vec{B}\) со скоростью \(\vec{v}\)):
\begin{equation}
    \vec{F} = q[\vec{v}\vec{B}]
\end{equation}

Модуль вектора \(\vec{B}\):
\begin{equation}
    B = qvB\sin{\alpha}
\end{equation}

Формула Лоренца (результирующая сила, действующая на движущийся заряд \(q\), если на него действует электростатическое поле напряжённостью \(\vec{E}\) и магнитное индукцией \(\vec{B}\)):
\begin{equation}
    \vec{F} = q\vec{E} + q[\vec{v}\vec{B}]
\end{equation}

Холловская поперечная разность потенциалов:
\begin{equation}
    \Delta\varphi = R_H\frac{IB}{d}
\end{equation}
\(d\) --- толщина пластины;
\(R_H\) --- постоянная Холла:
\begin{equation}
    R_H = \frac{1}{en}
\end{equation}
\(e\) --- элементарный заряд;
\(n\) --- концентрация электронов.

Закон полного тока для магнитного поля в вакууме, следующий из теоремы о циркуляции вектора магнитной индукции \(\vec{B}\):
\begin{equation}
    \ointegral[L]{\vec{B}}{\vec{\ell}} = \ointegral[L]{B_{\ell}}{\ell} = \mu_0 \Sum[k=1][n]{I_k}
\end{equation}

Магнитная индукция внутри соленоида в ваккуме, имеющего \(N\) витков:
\begin{equation}
    B = \mu_0 \frac{N I}{\ell}
\end{equation}
\(\ell\) --- длина соленоида.

Магнитная индукция внутри тороида в ваккуме, имеющего \(N\) витков:
\begin{equation}
    B = \mu_0 \frac{N I}{2 \pi r}
\end{equation}
r --- внутренний радиус тороида.

Поток вектора магнитной индукции, он же магнитный поток:
\begin{align}
    &\dd\Phi_B = \vec{B} \dd\vec{S} = B_n \dd{S}
    &\qquad \text{(сквозь площадку \(\dd{S}\))}
    &\\
    &\Phi_B = \integral[S]{\vec{B}}{\vec{S}} = \integral[S]{B_n}{S}
    &\qquad \text{(сквозь поверхность \(S\))}
    &\\
    &\Phi_B = \ointegral[S]{\vec{B}}{\vec{S}} = \ointegral[S]{B_n}{S}
    &\qquad \text{(сквозь замкнутую поверхность \(S\))}
\end{align}

Элементарная работа по перемещению проводника и замкнутого контура с током в магнитном поле:
\begin{align}
    \dd{A} = I \dd\Phi
    \qquad \dd{A} = I \dd\Phi'
\end{align}
\(\dd\Phi\) --- магнитный поток, пересекаемый движущимся проводником;
\(\dd\Phi'\) --- изменение магнитного потока, сцепленного с контуром.

Закон Фарадея (ЭДС электромагнитной индукции):
\begin{equation}
    \Emf_{i} = \frac{\dd{\Phi}}{\dd{t}}
\end{equation}

ЭДС индукции, возникающая в рамке прощадью \(S\) при вращении рамки с угловой скоростью \(\omega\) в однородном магнитном поле с индукцией \(B\):
\begin{equation}
    \Emf_{i} = B S \omega \sin{\omega t}
\end{equation}
\(\omega t\) --- мгновенное значение угла между вектором \(\vec{B}\) и вектором нормали \(\vec{n}\) к плоскости рамки.

Магнитный поток, создаваемый током \(I\) в контуре:
\begin{equation}
    \Phi = L I
\end{equation}
\(L\) --- индуктивность контура.

Закон Фарадея применительно к самоиндукции:
\begin{equation}
    \Emf_{s} = -L\frac{\dd{I}}{\dd{t}}
\end{equation}

Индуктивность соленоида:
\begin{equation}
    L = \mu_0 \mu \frac{N^2 S}{\ell}
\end{equation}
\(N\) --- число витков соленоида;
\(\ell\) --- его длина;
\(S\) --- площадь поперечного сечения.

Сила тока при размыкании и замыкании цепи, содержащей источник тока \(I_0\), резистор сопротивлением \(R\) и катушку индуктивностью \(L\):
\begin{align}
    &I = I_0 \exp(-\frac{R}{L}t) = I_0 \exp(-\frac{t}{\tau})
    &qquad \text{(размыкание)}
    &\\
    &I = I_0 (1-\exp(-\frac{R}{L}t)) = I_0 (1-\exp(-\frac{t}{\tau}))
    &qquad \text{(замыкание)}
\end{align}
\(\tau\) --- время релаксации:
\begin{equation}
    \tau = \frac{L}{R}
\end{equation}

ЭДС взаимной индукции:
\begin{equation}
    \Emf = -L\frac{\dd{I}}{\dd{t}}
\end{equation}

Взаимная индуктивность двух катушек с числом витков \(N_1\) и \(N_2\), намотанных на общий тороидальный сердечник:
\begin{equation}
    L = \mu_0 \mu \frac{N_1 N_2}{\ell} S
\end{equation}
\(ell\) --- длина сердечника по средней линии;
\(S\) --- площадь поперечного сечения сердечника.

Коэффициент трансформации:
\begin{equation}
    \frac{N_2}{N_1} = \frac{\Emf_2}{\Emf_1} = \frac{I_1}{I_2}
\end{equation}
\(N\), \(\Emf\), \(I\) --- соответственно число витков, ЭДС и сила тока в обмотках трансформатора.

Энергия магнитного поля создаваемого током в замкнутом контуре индуктивностью \(L\) по которому течёт ток силой \(I\):
\begin{equation}
    W = \frac{L I^2}{2}
\end{equation}

Объёмная плотность энергии однородного магнитного поля длинного соленоида:
\begin{equation}
    w = \frac{W}{V} = \frac{B^2}{2 \mu_0 \mu} = \frac{\mu_0 \mu H^2}{2} = \frac{B H}{2}
\end{equation}
V --- объём соленоида;
B --- магнитная индуктивность;
H --- напряжённость магнитного поля.

Связь между орбитальным магнитным \(\vec{m}\) и орбитальным механическим \(\vec{L}_{\ell}\) моментами электрона:
\begin{equation}
    \vec{m} = -\frac{e}{2m_e} \vec{L}_{\ell} = g \vec{L}_{\ell}
\end{equation}
\(g\) --- гиромагнитное отношение электрона;
\(m_e\) --- масса электрона.

Намагниченность:
\begin{equation}
    \vec{J} = \frac{m}{V}
\end{equation}

Связь между намагниченностью \(\vec{J}\) и напряжённостью \(\vec{H}\) магнитного поля:
\begin{equation}
    \vec{J} = \chi \vec{H}
\end{equation}
\(\chi\) --- магнитная восприимчивость вещества.

Связь между векторами \(\vec{B}\), \(\vec{H}\) и \(\vec{J}\):
\begin{equation}
    \vec{B} = \mu_0 (\vec{H} + vec{J}) = \mu_0 (\vec{H} + \chi \vec{J}) = \mu_0 (1+\chi)\vec{H}
\end{equation}

Связь между магнитной проницаемостью и магнитной восприимчивостью вещества:
\begin{equation}
    \mu = 1+\chi
\end{equation}

Теорема о циркуляции вектора магнитной индукции \(\vec{B}\):
\begin{equation}
    \ointegral[L]{\vec{B}}{\vec{\ell}} = \ointegral[L]{B_{\ell}}{\ell} = \mu_0 (I+I')
\end{equation}

Теорема о циркуляции вектора напряжённости \(\vec{H}\):
\begin{equation}
    \ointegral[L]{\vec{H}}{\vec{\ell}} = I
\end{equation}

Условия на границе двух магнетиков при отсутствии на границе тока:
\begin{align}
    &H_{1 \tau} = H_{2 \tau} \qquad \frac{B_{1 \tau}}{B_{2 \tau}} = \frac{\mu_1}{\mu_2}
    &\\
    &B_{1 n} = B_{2 n} \qquad \frac{H_{1 n}}{H_{2 n}} = \frac{\mu_2}{\mu_1}
\end{align}
\(H_{\tau}\) и \(B_{\tau}\) --- тангенциальная составляющая векторов \(\vec{H}\) и \(\vec{B}\);
\(H_n\) и \(B_n\) --- нормальная составляющая векторов \(\vec{H}\) и \(\vec{B}\).

Циркуляция вектора напряжённости \(\vec{E}_B\) поля, возбуждаемого переменным магнитным полем:
\begin{equation}
    \ointegral[L]{\vec{E}_B}{\vec{\ell}} = - \integral[S]{\fdv{\vec{B}{t}}}{\vec{S}}
\end{equation}

Плотность тока смещения:
\begin{equation}
    \vecj_{D} = \dv{\vec{D}}{t} = \varepsilon_0 \dv{\vec{E}}{t} + \dv{\vec{P}}{t}
\end{equation}
\(\vec{D}\) --- электрическое смещение;
\(\varepsilon \fdv{\vec{E}}{t}\) --- плотность тока смещения в ваккуме;
\(\fdv{\vec{P}}{t}\) --- плотность тока поляризации.

Полная плотность тока:
\begin{equation}
    \vecj_0 = \vecj + \vecj_{D}
\end{equation}
\(\vecj\) --- плотность тока проводимости.

Обобщённая теорема о циркуляции вектора \(\vec{H}\)
\begin{equation}
    \ointegral[L]{\vec{H}}{\vec{\ell}} = \integral[S]{\vecj + \vecj_{D}}{\vec{S}}
\end{equation}

Уравнения Максвелла в интегральной форме:
\begin{align}
    &\ointegral[L]{\vec{E}_B}{\vec{\ell}} = - \integral[S]{\fdv{\vec{B}{t}}}{\vec{S}}
    &\quad 
    &\ointegral[S]{\vec{D}}{\vec{S}} = \integral[V]{\rho}{V}
    &\\
    &\ointegral[L]{\vec{H}}{\vec{\ell}} = \integral[S]{\vecj + \vecj_{D}}{\vec{S}}
    &\quad
    &\ointegral[S]{\vec{B}}{\vec{S}} = 0
\end{align}

Уравнения Максвелла в дифференциальной форме:
\begin{align}
    &\nabla \times \vec{E} = -\fdv{\vec{B}}{t}
    &\nabla \cdot \vec{D} = \rho
    &
    &\\
    &\rot{\vb{H}} = \vecj + \vecj_{D}
    &\div{\vb{B}} = 0
\end{align}

Уравнения Максвелла для стационарных полей:
\begin{align}
    &\ointegral[L]{\vec{E}}{\vec{\ell}} = 0 \qquad \ointegral[S]{\vec{D}}{\vec{S}} = Q
    &\\
    &\ointegral[L]{\vec{H}}{\vec{\ell}} = I \qquad \ointegral[S]{\vec{B}}{\vec{S}} = 0
\end{align}