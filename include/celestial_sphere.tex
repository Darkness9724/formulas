\section{Небесная сфера}

Склонение светила~\cite{bakulin}:
\begin{equation}
    \delta_{\odot} = \sin^{-1}\qty{\sin{I}\sin\qty[\frac{\ang{360}}{P}(d-81)]} = I \sin\qty[\frac{\ang{360}}{P}(d-81)] = -I \cos\qty(\frac{\ang{360} \cdot n}{P})
\end{equation}
\(d\) --- текущий день в году;
\(P\) --- сидерический период в днях, вычисляемый по третьему закону Кеплера~\ref{eq:kepler};
\(n\) --- число дней прошедших с последнего зимнего солнцестояния.

Высота Солнца над горизонтом в полдень~\cite{bakulin}:
\begin{equation}
    h_{\odot} = \delta_{\odot} \pm (\ang{90}-\varphi)
\end{equation}
\(\varphi\) --- широта наблюдателя. Плюс или минус ставится в зависимости от северного или южного полушария.

Часовой угол:
\begin{equation}
    H = T_s - h_{\odot}
\end{equation}
\(T_s\) --- местное звёздное время, выраженное в тех же единицах, что \(H\) и \(h_{\odot}\).

Продолжительность светового дня~\cite{bakulin}:
\begin{align}
    &T_{д} = \frac{24}{\pi} \arccos(-\tan{\varphi} \tan{\delta_{\odot}})
    &\qquad \text{(через радианы)}
    &\\
    &T_{д} = 24 - 2\arccos\qty(\frac{\tan{\varphi} \tan{\delta_{\odot}}}{\ang{15}})
    &\qquad \text{(через градусы)}
\end{align}

Азимуты точек восхода и захода:
\begin{align}
    &A_{зах} = \arccos\qty(\frac{\sin{\delta_{\odot}}}{\cos{\varphi}})
    &\\
    &A_{восх} = \ang{180} - A_{зах}
\end{align}

\begin{figure}
    \centering
    \includesvg[scale=0.6]{equat_coord}
    \includesvg[scale=0.7]{horiz_coord}
    \caption{Экваториальная и горизонтальная системы координат}
\end{figure}

\begin{figure}
    \centering
    \includesvg[scale=0.6]{first_triangle}
    \includesvg[scale=0.6]{second_triangle}
    \includesvg[scale=0.7]{third_triangle}
    \caption{Первый, второй и третий астрономические треугольники для перевода систем координат}\label{fig:triangle}
\end{figure}

В соответствии с~\Autoref{fig:triangle}, переход от первой экваториальной системы к горизонтальной:
\begin{align}
    &\sin{h} = \sin{\varphi} \sin{\delta}\cos{\varphi} \cos{\delta} \cos{H}
    &\\
    &\sin{A} \cos{h} = -\cos{\delta} \sin{H}
    &\\
    &\cos{A} \cos{h} = \cos{\varphi} \sin{\delta} - \sin{\varphi} \cos{\delta} \cos{H}
\end{align}

Переход от горизонтальной системы к первой экваториальной:
\begin{align}
    &\sin{\delta} = \sin{\varphi} \sin{h} + \cos{\varphi} \cos{h} \cos{H}
    &\\
    &\sin{H} \cos{\delta} = -\cos{h} \sin{A}
    &\\
    &\cos{H} \cos{\delta} = \cos{\varphi} \sin{h} - \sin{\varphi} \cos{h} \cos{A}
\end{align}

Переход от второй экваториальной системы к эклиптической:
\begin{align}
    &\sin{B} = \sin{\delta} \cos{\epsilon} - \cos{\delta}\sin{\epsilon}\sin{\alpha}
    &\\
    &\cos{B} \cos{\lambda} = \cos{\delta}\cos{\alpha}
    &\\
    &\cos{B} \sin{\lambda} = \sin{\delta} \sin{\epsilon} + \cos{\delta} \cos{\epsilon} \sin{\alpha}
\end{align}

Переход от эклиптической системы ко второй экваториальной:
\begin{align}
    &\sin{\delta} = \sin{\epsilon} \sin{\lambda} \cos{B} + \cos{\epsilon}\sin{B}
    &\\
    &\cos{\delta} \cos{\alpha} = \cos{\lambda}\cos{B}
    &\\
    &\cos{\delta} \sin{\alpha} = \cos{\epsilon} \sin{\lambda} \cos{B} - \sin{\epsilon} \sin{B}
\end{align}
\(\epsilon\) --- угол наклона эклиптики к небесному экватору.

Переход от второй экваториальной системы к галактической:
\begin{align}
    &\sin{b} = \sin{\delta} \sin{\delta'} + \cos{\delta} \cos{\delta'} \cos(\alpha - \alpha')
    &\\
    &\cos{b} \sin(l' - l) = \cos{\delta} \sin(\alpha - \alpha')
    &\\
    &\cos{b} \cos(l' - l) = \cos{\delta'}\sin{\delta} - \sin{\delta'}\cos{\delta} \cos(\alpha - \alpha')
\end{align}

Переход от галактической системы ко второй экваториальной:
\begin{align}
    &\sin{\delta} = \sin{b} \sin{\delta'} + \cos{b} \cos{\delta'} \cos(l' - l)
    &\\
    &\cos{\delta} \sin(\alpha - \alpha') = \cos{b} \sin(l' - l)
    &\\
    &\cos{\delta} \cos(\alpha - \alpha') = \cos{\delta'}\sin{b} - \sin{\delta'}\cos{b} \cos(l' - l)
\end{align}
\(\alpha\) --- прямое восхождения светила;
\(b\) --- его галактическая широта;
\(l\) --- его галактическая долгота;
\(\alpha'\), \(b'\), \(l'\) --- эти же координаты для северного полюса мира. Определяются по эпохе.

Измерение расстояния методом параллакса:
\begin{equation}
    \tan{\theta} = \frac{x}{D} \sim \theta \sim \sin{\theta}
\end{equation}
Угол параллакса \(\theta\) выражен в радианах и равен половине угла сдвига между двумя наблюдательными пунктами;
\(x\) --- половина базового расстояния между этими пунктами и расстоянием от наблюдателя до звезды.

Связь параллакса \(P\) в арксекундах с парсеками \(D\):
\begin{equation}
    D = \frac{1}{P}
\end{equation}

Телесный угол \(\Omega\):
\begin{align}
    \dd{\Omega} = \sin{\theta}\dd{\theta}\dd{\phi}
    \qquad
    \Omega = \integral[S]{\sin{\theta}}{\theta}\dd{\phi}
\end{align}

Угловое расстояние между двумя точками на сфере:
\begin{equation}
    \Psi = \arccos(\sin{\theta_1} \sin{\theta_2} + \cos{\theta_2} \cos{\theta_2} \cos{\phi_1 - \phi_2})
\end{equation}