\section{Релятивистская механика}

Гравитационный радиус сферического невращающегося тела:
\begin{equation}
    r_{г} = \frac{2G M}{c^2}
\end{equation}
\(c\) --- скорость света (\nameref{sec:const_list}).

Уравнение Мещерского в релятивистской механике в случае отделения частиц со скоростью коллинеарной скорости ракеты:
\begin{equation}
    m\frac{\dd{v}}{\dd{t}} = -\qty(1 - \frac{v^2}{c^2})  u \frac{\dd{m}}{\dd{t}}
\end{equation}
\(u\) --- скорость частиц относительно ракеты:
\begin{equation}
    u = \frac{v_2 - v}{1-v_2  v/c^2}
\end{equation}

\subsection{Метрика Шварцшильда}
Для невращающегося точечного тела.

Замедление времени в гравитационном радиусе этого тела:
\begin{align}
    \Delta t' = \Delta t \sqrt{1-\frac{r_{г}}{r}}
    \qquad \text{для неподвижного наблюдателя}
\end{align}
\begin{align}
\Delta t' = \Delta t \sqrt{1-\frac{3r_{г}}{2r}}
\qquad \text{для наблюдателя на круговой орбите}
\end{align}
\(\Delta t'\) --- промежуток времени межу событиями по часам наблюдателя;
\(\Delta t\) --- тот же промежуток времени по часам вне поля тяготения;
\(r\) --- расстояние между наблюдателем и центром тяготения.

Искажение пространства в поле тяготения:
\begin{equation}
    \Delta r' = \frac{\Delta r}{\sqrt{1 - r_{г}/r}}
\end{equation}
Ускорение свободного падения в релятивистской механике при \(r \gg r_{г}\):
\begin{equation}
    g = \frac{G M}{r^2 \sqrt{1 - r_{г}/r}}
\end{equation}
Ускорение приливной силы, действующей на элемент длины \(\ell\) при \(\ell \ll r\) и \(𝑟 \gg 𝑟_{г}\):
\begin{equation}
    a \approx \ell \frac{2G M}{r^3} \qty(1+\frac{3 r_{г}}{4 r})
\end{equation}
Отклонение потока фотонов (луча света) в поле тяготения:
\begin{equation}
    \Delta\Phi = \frac{4 G M}{c^2 r}
\end{equation}
\(r\) --- расстояние между центром тяготения и траекторией потока фотонов.

Средняя ''плотность'' чёрной дыры, заключённая под горизонтом событий, обратно пропорциональна её массе:
\begin{equation}
    p = \frac{3 c^6}{32\pi M^2 G^3} = \frac{3}{4\pi r_{г}^3}
\end{equation}

Радиус наименьшей стабильной орбиты вокруг чёрной дыры Шварцшильда (аккреционный диск не может быть ниже этой орбиты):
\begin{equation}
    r_{\min} = 3 r_{г}
\end{equation}
Эффективность преобразование массы покоя в излучение для аккреционного диска:
\begin{equation}
    L = \frac{2\eta G M m}{R}
\end{equation}
Температура аккреционного диска:
\begin{equation}
    T(R) = \sqrt{4}{\frac{3 G M m}{8 \pi R^3 \sigma}\qty(1 - \sqrt{\frac{R_0}{R}})}
\end{equation}
\(R_0\) --- радиус аккреции;
\(R\) --- радиус Шварцшильда;
\(\sigma\) --- постоянная Стефана-Больцмана.

При \(K < K_{\max}\) звезда коллапсирует в чёрную дыру по окончанию жизненного цикла:
\begin{align}
    &K = j M R^2 \omega
    &\\
    &K_{\max} = \frac{2G M^2}{c}
\end{align}
\(j\) --- безразмерный момент инерции (для Солнца \(j = 0.059\)).

Объём Шварцшильда обратен средней плотности чёрной дыры:
\begin{equation}
    V_{Ш} = \frac{4\pi r_{г}^3}{3}
\end{equation}

Время, за которое чёрная дыра испарится в результате излучения Хокинга:
\begin{align}
    &t = \frac{5120\pi G^2 M_0^3}{\hbar c^4}
    &\\
    &t = \frac{480\pi c^2 V_{Ш}}{\hbar G}
\end{align}
\(\hbar\) --- постоянная Дирака (\nameref{sec:const_list}).

Мощность излучения Хокинга:
\begin{equation}
    P = \frac{\hbar c^6}{15360\pi G^2 M^2}
\end{equation}
Время, за которое наблюдатель с нулевой начальной скоростью на расстоянии \(r_0\), упадёт на горизонт событий:
\begin{align}
    t = \frac{3}{2} \frac{r_{г}}{c} \qty[\qty(\frac{r_0}{r_{г}})^{\frac{3}{2}} - 1]
    \qquad \text{по часам падающего наблюдателя.}
\end{align}
\begin{align}
    t = \infty
    \qquad \text{по часам стороннего наблюдателя время падения никогда не наступит.}
\end{align}
Давление вырожденного газа в релятивистской механике:
\begin{equation}
    p = \qty[\frac{1}{8} \frac{3}{\pi} \frac{h c}{m_e m_p^{\frac{4}{3}} \mu_e^{\frac{4}{3}}}] p^{\frac{4}{3}}
\end{equation}
Продольный эффект Доплера в релятивистской механике:
\begin{equation}
    \frac{\Delta \lambda}{\lambda} = \frac{v}{c} \qty(\sqrt{\frac{1+v/c}{1-v/c}}-1)
\end{equation}
Коротковолновая граница сплошного рентгеновского спектра:
\begin{equation}
    \lambda_{\min} = \frac{ch}{eU}
\end{equation}
\(U\) --- разность потенциалов, приложенная к рентгеновской трубке.

Закон Мозли, определяющий частоты спектральных линий характеристического рентгеновского излучения:
\begin{equation}
    v = R(Z-\sigma)^2\qty(\frac{1}{m^2}-\frac{1}{n^2})
\end{equation}
\(R\) --- постоянная Ридберга;
\(Z\) --- порядковый номер элемента в периодической таблице;
\(\sigma\) --- постоянная экранирования;
\(m\) --- рентгеновская серия (\(m = 1\), \(2\), \(3\), \(\ldots\)), \(n\) --- линии, соответствующие серии \(m\) (\(n = m + 1\), \(m + 2\), \(m + 3\), \(\ldots\)).

Преобразование Лоренца в сравнении с преобразованием Галилея~\eqref{eq:galilean}:
\begin{equation}
    \begin{cases}
        x' = \frac{x-vt}{\sqrt{1-v^2/c^2}}\\
        y' = y\\
        z' = z\\
        t' = \frac{t-(vx)/c^2}{\sqrt{1-v^2/c^2}}
    \end{cases}
\end{equation}
Релятивистское замедление хода часов:
\begin{equation}
    \Delta t' = \frac{\Delta t}{\sqrt{1-v^2/c^2}}
\end{equation}
Релятивистское сокращение длины:
\begin{equation}
    l' = l_0\sqrt{1-v^2/c^2}
\end{equation}

Релятивистский закон сложения скоростей:
\begin{align}
    {v'}_x = \frac{v_x - v}{1-vv_x/c^2}
    \qquad {v'}_y = \frac{v_y\sqrt{1-v^2/c^2}}{1-vv_x/c^2}
    \qquad {v'}_z = \frac{v_z\sqrt{1-v^2/c^2}}{1-vv_x/c^2}
\end{align}

Интервал между событиями:
\begin{equation}
    \tau = \sqrt{c^2 t^2 - \ell^2}
\end{equation}
\(t\) --- промежуток времени между этими событиями;
\(\ell\) --- расстояние между точками, где произошло событие.

Релятивистский импульс частицы:
\begin{equation}
    \vec{p} = \frac{m\vec{v}}{\sqrt{1-v^2/c^2}}
\end{equation}
\(m\) --- масса частицы;
\(v\) --- её скорость.

Импульс фотона:
\begin{equation}
    p = \frac{hv}{c}
\end{equation}
Основной закон релятивистской динамики:
\begin{equation}
    F = \frac{\dd{\vec{p}}}{\dd{t}}
\end{equation}
Энергия покоя частицы:
\begin{equation}
    E_0 = mc^2
\end{equation}
\(m\) --- масса покоя частицы.

Полная энергия релятивистской частицы:
\begin{equation}
    K = \frac{mc^2}{\sqrt{1-v^2/c^2}}
\end{equation}
Кинетическая энергия релятивистской частицы:
\begin{equation}
    K = E - E_0 = mc^2\qty(\frac{1}{\sqrt{1-v^2/c^2}}-1)
\end{equation}
Связь между энергией и импульсом релятивистской частицы:
\begin{align}
    E^2 = m^2 c^4 + p^2 c^2
    \qquad pc = \sqrt{K(K+2mc^2)}
\end{align}

\subsection{Метрика Керра}
Для вращающегося тела точечной массы.

Замедление времени:
\begin{equation}
    \Delta t' = \Delta t \sqrt{1 - r \frac{r_{г}}{r^2 + (K/M c)^2 \sin^2{\varphi}}}
\end{equation}
\(\varphi\) --- широта наблюдателя (если \(\varphi = \ang{0}\), то формула аналогична замедлению времени в метрике Шварцшильда);
\(L\) --- момент импульса центрального тела:
\begin{equation}
    L = I \omega
\end{equation}
\(I\) --- момент инерции.

Полярный гравитационный радиус вращающейся чёрной дыры Керра:
\begin{equation}
    r_{К} = \frac{r_{г}}{2} \qty[1+\sqrt{1-\qty(\frac{2 K}{K_{\max}})^2}]
\end{equation}