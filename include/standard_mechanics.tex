\section{Стандартная механика}

\subsection{Кинематика}

Преобразование Галилея:
\begin{equation}
    \begin{cases}\label{eq:galilean}
        x' = x + vt\\
        y' = y\\
        z' = z\\
        t' = t
    \end{cases}
\end{equation}

Средняя и мгновенная скорость материальной точки:
\begin{align}
    \langle\vec{v}\rangle = \frac{\Delta\vec{r}}{\Delta t}
    \qquad \vec{v} = \frac{\dd{\vec{r}}}{\dd{t}}
\end{align}
\(\Delta\vec{r}\) --- элементарное перемещение точки за промежуток времени \(\Delta t\);
\(\vec{r}\) --- радиус-вектор точки.

Среднее и мгновенное ускорение материальной точки:
\begin{align}
    \langle\vec{a}\rangle = \frac{\Delta\vec{v}}{\Delta t}
    \qquad \vec{a} = \frac{\dd{\vec{v}}}{\dd{t}}
\end{align}

Тангенциальная (касательная) и нормальная составляющая ускорения:
\begin{align}
    a_t = \frac{\dd{\vec{v}}}{\dd{t}}
    \qquad a_n = \frac{v^2}{r}
\end{align}
\(r\) --- радиус кривизны траектории.

\begin{longtable}{ccc}
    \caption{Классификация движения в зависимости от тангенциальной и нормальной составляющей ускорения}\\
    \toprule
    \thead{\(a_t\)} & \thead{\(a_n\)} & \thead{Движение} \\
    \midrule
    \{\(0\), const\} & 0 & Прямолинейное равномерное \\
    \(f(t)\) & 0 & Прямолинейное переменно ускоренное \\
    0 & const & Равномерное по окружности \\
    \{0, const\} & \(\neq 0\) & Криволинейное равномерное \\
    \(f(t)\) & \(\neq 0\) & Криволинейное переменно ускоренное \\
    \bottomrule
\end{longtable}

Полное ускорение при криволинейном движении:
\begin{equation}
    \vec{a} = \vec{a_t} + \vec{a_n}
\end{equation}

Путь и скорость для равнопеременного движения:
\begin{align}
    s = v_0 t \pm \frac{a t^2}{2}
    \qquad v = v_0 \pm a t
\end{align}

Длина пути, пройденного материальной точкой за промежуток времени с \(t_1\) до \(t_2\):
\begin{equation}
    s = \integral[t_1][t_2]{v(t)}{t}
\end{equation}

Путь, пройденный телом в свободном падении при нулевой начальной скорости:
\begin{equation}
    h = \frac{g t^2}{2}
\end{equation}

Скорость тела в произвольный момент времени \(t\):
\begin{equation}
    \vec{v} = \vec{g}   t
\end{equation}

Модуль скорости тела при падении с высоты \(h\):
\begin{equation}
    v = \sqrt{2 g h}
\end{equation}

Время падения тела с высоты h при нулевой начальной скорости:
\begin{equation}
    t = \sqrt{\frac{2h}{g}}
\end{equation}

Движение тела брошенного вертикально вверх с начальной скоростью \(v_0\) в соответствии с~\Autoref{fig:vertical}:
\begin{equation}
    \begin{cases}
        0 = v_0 - g t\\
        0 - v^2 = -2g t
    \end{cases}
\end{equation}

Его время падения:
\begin{equation}
    t = \frac{v_0}{g}
\end{equation}

Высота подъёма:
\begin{equation}
    h = \frac{v_0^2}{2g}
\end{equation}

Свободное падение от максимальной точки подъёма:
\begin{equation}
    {v'}^2 - 0 = 2gh \Rightarrow {v'}^2 = 2g\frac{v_0^2}{2g}
\end{equation}

\begin{wrapfigure}{r}{0.5\textwidth}
    \caption{Движение тела брошенного вертикально вверх}\label{fig:vertical}
    \centering
    \tikzsetnextfilename{vertical_movement}
    \begin{tikzpicture}
        \draw[thick, -Stealth, black] (0, 0) -- (0, 4);
        \path[black] (0,4) node [right]{\(y\)};
        \path[black] (0,3) node [right]{\(v_t = 0\)};
        \draw[-, black] (-2, 3) -- (0, 3);
        \path[black] (-2,3) node [above]{\(v_0 = 0\)};
        \draw[Stealth-Stealth, black] (-1, 3) -- (-1, 0);
        \path[black] (-1,3/2) node [left]{\(h\)};
        \draw[ultra thick, -Stealth, black] (0, 0) -- (0, 1);
        \path[black] (0,1) node [right]{\(\vec{v}_0\)};
        \draw[ultra thick, -Stealth, black] (0, 0) -- (0, -1);
        \path[black] (0,-1) node [right]{\(\vec{v}_t\)};
        \draw[-, black] (-2, 0) -- (2, 0);
        \path[black] (-2,0) node [below]{\(x\)};
    \end{tikzpicture}
\end{wrapfigure}

Конечная скорость падения \(v'\) равна начальной скорости бросания \(v_0\):
\begin{equation}
    v' = v_0 = 0 + gt'
\end{equation}

Время падения равно времени подъёма:
\begin{equation}
    t' = \frac{v'}{g} = \frac{v_0}{g} = t
\end{equation}

В соответствии с~\Autoref{fig:horizontal} движение тела брошенного горизонтально с начальной скоростью \(v_0\) с высоты \(h\) следует рассматривать как комбинацию горизонтального равномерного движения со скоростью \(v_0\) и вертикального свободного падения \(g\):
\begin{align}
    x = v_0 t
    \qquad y = \frac{gt^2}{2}
\end{align}

Уравнением траектории тела будет парабола:
\begin{equation}
    y = \frac{g}{2 v_0}x^2
\end{equation}

Горизонтальная дальность полёта:
\begin{equation}
    s = v_0 t = v_0\sqrt{\frac{2h}{g}}
\end{equation}

Мгновенная скорость и её модуль в каждой точки траектории:
\begin{align}
    \vec{v} = \vec{v_0} + \vec{g}t
    \qquad v = \sqrt{v_0^2 + g^2 t^2}
\end{align}

\begin{wrapfigure}{r}{0.5\textwidth}
    \caption{Движение тела брошенного к горизонту}\label{fig:horizontal}
    \centering
	\includegraphics{horizontal}
    % \tikzsetnextfilename{horizontal_movement}
    % \begin{tikzpicture}
    %     \draw[thick, -Stealth] (-0.5, 0) -- (8, 0);
    %     \path (8, 0) node [above]{\(x\)};
    %     \draw[thick, -Stealth] (0, 0.5) -- (0, -6);
    %     \path (0, -6) node [right]{\(y\)};
    %     \path (0, 1) node[right]{\(0\)};
    %     \draw[ultra thick, -Stealth] (0, 0) -- (2, 0);
    %     \path (2, 0) node [above]{\(\vec{v}_0\)};
    %     \draw[thick, -Circle] (0,0) parabola[bend at start] (8,-6);
    %     \draw[ultra thick, -Stealth] (6,-3) -- (8,-3);
    %     \draw[ultra thick, -Stealth] (6,-3) -- (6,-5);
    % \end{tikzpicture}
\end{wrapfigure}

В соответствии с~\Autoref{fig:hangle} движение тела брошенного под углом \(\alpha\) к горизонту с начальной скоростью \(v_0\) следует рассматривать как комбинацию горизонтального равномерного движения со скоростью \(\vec{v}_x\) и движение тела, брошенного вертикально вверх со скоростью \(\vec{v}_y\):
\begin{align}
    v_x = v_0\cos{\alpha}
    \qquad v_y = v_0\sin{\alpha} - gt
\end{align}

Модули мгновенной скорости в каждой точки траектории при подъёме и спуске:
\begin{align}
    &v_{п} = \sqrt{v_x^2 + v_y^2} = \sqrt{(v_0\cos{\alpha})^2+(v_0\sin{\alpha} - gt)^2}
    &\\
    &v_{сп} = \sqrt{v_0x^2 + (gt)^2} = \sqrt{(v_0\cos{\alpha})^2+(gt)^2}
\end{align}
\(v_0x = v_0\cos{\alpha}\) --- проекция начальной скорости на оси координат.

Время подъёма:
\begin{equation}
    t_{п} = \frac{v_0\sin{\alpha}}{g}
\end{equation}

Общее время движения:
\begin{equation}
    t = \frac{2v_0\sin{\alpha}}{g}
\end{equation}

Дальность полёта тела:
\begin{equation}
    s = \frac{v_0\sin{2\alpha}}{g}
\end{equation}

Максимальная высота подъёма:
\begin{equation}
    h_{\max} = \frac{v_0^2\sin^2{\alpha}}{2g}
\end{equation}

\begin{wrapfigure}{r}{0.5\textwidth}
    \caption{Движение тела брошенного под углом к горизонту}\label{fig:hangle}
    \centering
	\includegraphics[scale=0.5]{hangle}
\end{wrapfigure}

Угловая скорость:
\begin{equation}
    \vec{\omega} = \frac{\dd{\vec{\varphi}}}{\dd{t}}
\end{equation}

Угловая скорость равномерного вращательного движения:
\begin{equation}\label{eq:angular velocity}
    \omega = \frac{\varphi}{t} = \frac{2\pi}{T} = 2\pi n
\end{equation}
\(\varphi\) --- угол поворота произвольного радиуса от начального движения;
\(t\) --- промежуток времени, за который произошёл данный поворот;
\(T\) --- период вращения;
\(n\) --- частота вращения.

Угловое ускорение:
\begin{equation}
    \vec{\epsilon} = \frac{\dd{\vec{\omega}}}{\dd{t}}
\end{equation}

Кинематическое уравнение равнопеременного движения:
\begin{equation}
    \varphi = \varphi_0 + \omega t
\end{equation}
\(\varphi_0\) --- начальное угловое перемещние.

Угол поворота и угловая скорость для равнопеременного вращательного движения:
\begin{align}
    \varphi = \omega_0 t \pm \frac{\epsilon t^2}{2}
    \qquad \omega = \omega_0 \pm \epsilon t
\end{align}
\(\omega_0\) --- начальная угловая скорость.

Линейные величины связаны с угловыми уравнениями:
\begin{align}
    s = R\varphi
    \qquad v = R\omega
\end{align}
\begin{align}
    a_t = R\epsilon
    \qquad a_n = R\omega^2
\end{align}
Правило буравчика для угловых скоростей:
\begin{equation}
    \vec{v} = \vec{\omega} \vec{r}
\end{equation}
Оно же для момента импульса:
\begin{equation}
    \vec{v} = \frac{\vec{L} \vec{r}}{I}
\end{equation}
\(I\) --- момент инерции.

Оно же для момента силы:
\begin{equation}
    \vec{M} = \Sum[i]{\qty[\vec{r}\vec{F}]}
\end{equation}

\subsection{Динамика}
Сила трения качения:
\begin{equation}
    F_{тр} = \frac{f_k N}{R}
\end{equation}
\(f_{к}\) --- коэффициент трения качения, \(R\) --- радиус катящегося тела, \(N\) --- сила нормальной реакции:
\begin{equation}
    N = mg
\end{equation}

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \tikzsetnextfilename{rolling_drag}
    \begin{tikzpicture}
        \pgfmathsetmacro{\R}{2}
        \path ( 0:\R) coordinate (R);
        \path (0,-\R) coordinate (N);
        \draw[green,fill=gray!10] (0,0) circle (\R);
        \draw[black] (-2.5,-2) to (2.5,-2);
        \draw[-Stealth, green] +(80:\R) arc(80:-260:\R);
        \path[green] (-1,1) node [left]{\(f_{к}\)};
        \draw[red] (0,0) -- (R);
        \path[red] ( 0:\R/2) node [below]{\(R\)};
        \draw[-Stealth,blue] (0,0) -- (N);
        \path[blue] (0,0) node [left]{\(N\)};
        \draw[-Stealth,violet] (1,-\R) -- (-1,-\R);
        \path[violet] (0,-\R) node [below]{\(F_{тр}\)};
    \end{tikzpicture}
    \caption{Динамика катящегося тела}
\end{wrapfigure}

Максимальная сила трения покоя:
\begin{equation}
    F_{тр0} = f_0 N
\end{equation}
\(f_0\) --- коэффициент трения покоя.

Сила трения скольжения, он же закон Амонтона---Кулона:
\begin{equation}
    F_{тр} = \mu N \cos{\theta}
\end{equation}
\(\mu\) --- коэффициент трения скольжения;
\(\theta\) --- угол наклона.

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \includesvg[pretex=\tiny]{freebody}
    \caption{Сила трения скользящего тела по наклонной поверхности}
\end{wrapfigure}

Импульс (количество движения) материальной точки:
\begin{equation}
    \vec{p} = m\vec{v}
\end{equation}

Второй закон Ньютона, описывающий динамику материальной точки:
\begin{align}
    &\vec{a} = \frac{F}{m}
    &\\
    &\vec{F} = m\vec{a} = m\frac{\dd{\vec{v}}}{\dd{t}} = \frac{\dd{\vec{p}}}{\dd{t}}
\end{align}

Проекция второго закона Ньютона на касательную и нормаль к траектории:
\begin{align}
    F_t = m\frac{\dd{v}}{\dd{t}}
    \qquad F_n = \frac{mv^2}{r} = m\omega^2 R
\end{align}

Закон сохранения импульса для замкнутой системы:
\begin{equation}
    \vec{p} = \Sum[i=1][n]{m_i \vec{v_i}} = const
\end{equation}
\(n\) --- число материальных точек, входящих в систему;
\(m_i\) и \(\vec{м_i}\) --- масса и скорость i-точки.

Радиус-вектор центра масс системы из материальных точек:
\begin{equation}
    \vec{r} = \frac{\sum_{i=1}^n{m_i \vec{r_i}}}{\sum_{i=1}^n{m_i}}
\end{equation}
\(\vec{r_i}\) --- радиус-вектор i-точки.

Закон движения центра масс системы:
\begin{equation}
    m\frac{\dd{\vec{v}}}{\dd{t}} = \sum_{i=1}^n{\vec{F}}
\end{equation}

Уравнение Мещерского (реактивное движение тела переменной массы):
\begin{align}
    &m(t) \frac{\dd{\vec{v}}}{\dd{t}} = \vec{F} - \vec{u} \frac{\dd{m}}{\dd{t}}
    &\\
    &m \vec{a} = \vec{F} + \vec{F_p}
\end{align}
\(m(t)\) --- масса в момент времени \(t\);
\(v\) --- скорость в момент времени \(t\);
\(F_p\) --- реактивная сила;
\(u\) --- скорость истечения газов:
\begin{equation}
    u = \sqrt{2 g \frac{k}{k-1}  p_0  V_0 \qty[1-\qty(\frac{p}{p_d})^{\frac{k-1}{k}}]}
\end{equation}
\(p\) --- давление движущихся газов;
\(p_0\) --- абсолютное давление в камере сгорания;
\(p_d\) --- давление в выходном сопле;
\(k\) --- отношения теплоёмкостей газов:
\begin{equation}
    k = \frac{c_p}{c_v}
\end{equation}
\(с_p\) --- удельная теплоемкость газа при постоянном давлении;
\(с_v\) --- удельная теплоемкость газа при постоянном объеме.

Реактивная сила:
\begin{equation}
    \vec{F_p} = -\vec{u} \frac{\dd{m}}{\dd{t}}
\end{equation}

Ускорение тела переменной массы:
\begin{equation}
    \vec{a} = \frac{\vec{F_p} + \vec{F}}{m(t)}
\end{equation}

Формула Циолковского для определения скорости ракеты:
\begin{equation}
    v = u \ln\qty(\frac{m_0}{m(t)})
\end{equation}
\(m_0\) --- начальная масса ракеты;
\(m(t)\) --- масса ракеты в момент времени \(t\).

Угол поворота при гравитационном маневре:
\begin{equation}
    \varphi_g = \arctan\qty(\frac{v}{v'})^2
\end{equation}
\(v\) --- скорость аппарата до гравитационного манёвра;
\(v'\) --- его скорость после.

Момент инерции материальной точки:
\begin{equation}
    j = mr^2
\end{equation}
\(r\) --- расстояние до оси вращения.

Момент инерции системы тел:
\begin{equation}
    j = \Sum[i=1][n]{m_i r_i^2}
\end{equation}

Теорема Штейнера (момент инерции относительно параллельной оси, отстоящей от первой на расстоянии \(a\)):
\begin{equation}
    J = J_c + ma^2
\end{equation}
\(J_c\) --- моммент инерции относительно оси, проходящей через центр масс.

Кинетическая энергия системы тел:
\begin{equation}
    K \equiv \sum_i{m_i \frac{\dd{\vec{r_i}}}{\dd{t}} \vec{r_i}}
\end{equation}

Кинетическая энергия вращения тела:
\begin{equation}
    K_{вр} = \frac{1}{2}J_z\omega^2
\end{equation}
\(J_z\) --- момент инерции относительно оси \(z\), \(\omega\) --- его угловая скорость.

Момент силы относительно неподвижной оси \(z\):
\begin{equation}
    \vec{M_z} = [\vec{r}\vec{F}]_z
\end{equation}

Модуль вектора момента силы:
\begin{equation}
    M = Fl
\end{equation}
\(l\) --- плечо силы.

Работа при вращении тела:
\begin{equation}
    dA = M_z \dd{\varphi}
\end{equation}
\(M_z\) --- момент силы относительно неподвижной оси z, \(\varphi\) --- угол поворота тела.

Напряжение при упругой деформации:
\begin{equation}
    \sigma = \frac{F}{S}
\end{equation}
\(F\) --- растягивающая или сжимающая сила, \(S\) --- площадь поперечного сечения.

Относительное продольное растяжение или сжатие:
\begin{equation}
    \epsilon = \frac{\Delta\ell}{\ell}
\end{equation}
\(\Delta\ell\) --- изменение длины тела при растяжении или сжатии;
\(\ell\) --- длина тела до деформации.

Относительное поперечное растяжение или сжатие:
\begin{equation}
    \epsilon' = \frac{\Delta\ell}{\ell}
\end{equation}
\(\Delta d\) --- изменение диаметра стержня при растяжении или сжатии;
\(d\) --- диаметр стержня.

Связь между относительным поперечным растяжением или сжатием \(\epsilon'\) и относительным продольным растяжением или сжатием \(\epsilon'\) определяется коэффициентом Пуассона:
\begin{equation}
    \epsilon' = \mu\epsilon
\end{equation}

Закон Гука для продольного растяжения или сжатия:
\begin{equation}
    \sigma = E\epsilon
\end{equation}
\(E\) --- модуль Юнга:
\begin{equation}
    E = \frac{F\ell}{S\Delta\ell}
\end{equation}
\(F\) --- нормальная составляющая силы.

Потенциальная энергия упругорастянутого или упругосжатого стержня:
\begin{equation}
    \Pi = \frac{E\epsilon^2}{2}V
\end{equation}
\(V\) --- объём тела.

Теорема о вириале:
\begin{equation}
    \frac{1}{2} \frac{\dd{I}^2}{\dd{t^2}} = 2 K + \sum_i{\vec{F} \vec{r}}
\end{equation}

\subsection{Энергия}
        
Элементарная работа постоянной силы \(\vec{F}\), совершённой на перемещении \(\dd{\vec{r}}\):
\begin{equation}
    dA = \vec{F}\dd{\vec{r}} = F\cos{\alpha}\dd{s} = F_s \dd{s}
\end{equation}
\(\alpha\) --- угол между векторами \(\vec{F}\) и \(\dd{\vec{r}}\);
\(\dd{s} = \abs{\dd{\vec{r}}}\) --- элементарный путь;
\(F_s\) --- проекция силы \(\vec{F}\) на вектор \(\dd{\vec{r}}\).

Работа, совершаемая переменной силой на пути \(s\):
\begin{equation}
    A = \integral[s]{F_s}{s} = \integral[s]{F\cos{\alpha}}{s}
\end{equation}

Средняя мощность за промежуток времени \(t\):
\begin{equation}
    \langle N \rangle = \frac{\Delta A}{\Delta t}
\end{equation}

Мгновенная мощность:
\begin{equation}
    N = \frac{\dd{A}}{\dd{t}} = \vec{F}\vec{v} = F_s v = Fv\cos{\alpha}
\end{equation}
\(\vec{v}\) --- вектор скорости, с которой движется точка приложения силы \(\vec{F}\);
\(\alpha\) --- угол между векторами \(\vec{F}\) и \(\vec{v}\).

Закон Оберта, утверждающий, что чем быстрее движется тело, тем выше его КПД~\cite{oberth}:
\begin{equation}
    \frac{\dd{A}}{\dd{t}} = \frac{F}{m} v
\end{equation}
\(F\) --- тяга двигателя;
\(m\) --- масса ракеты;
\(v\) --- её скорость.

Кинетическая энергия движущегося тела:
\begin{equation}
    K = \frac{mv^2}{2}
\end{equation}

Потенциальная энергия тела, поднятого над поверхностью на высоту \(h\):
\begin{equation}
    \Pi = mgh
\end{equation}

Сила упругости:
\begin{equation}
    F = -k\chi
\end{equation}
\(\chi\) --- деформация;
\(k\) --- коэффициент упругости.

Потенциальная энергия упругодеформированного тела:
\begin{equation}
    \Pi = \frac{k\chi^2}{2}
\end{equation}

Закон сохранения энергии для замкнутой системы:
\begin{equation}
    E = K + \Pi = const
\end{equation}

Коэффициент восстановления:
\begin{equation}
    \epsilon = \frac{{v'}_n}{v_n}
\end{equation}
\(v_n\) и \({v'}_n\) --- нормальные составляющие относительной скорости тела до и после удара.

Скорость двух тел с массами \(m_1\) и \(m_2\) после прямого абсолютно упругого центрального удара:
\begin{align}
    &{v'}_1 = \frac{(m_1 - m_2)v_1+2 m_2 v_2}{m_1 + m_2}
    &\\
    &{v'}_2 = \frac{(m_2 - m_1)v_2+2 m_1 v_1}{m_1 + m_2}
\end{align}

Скорость двух тел с массами \(m_1\) и \(m_2\) после прямого абсолютно неупругого центрального удара:
\begin{equation}
    \vec{v} = \frac{m_1\vec{v}_1+m_2\vec{v}_2}{m_1 + m_2}
\end{equation}

Изменение кинетической энергии тел при абсолютно неупругом центральном ударе:
\begin{equation}
    \Delta K = \qty(\frac{m_1 v_1^2}{2}+\frac{m_2 v_2^2}{2}) - \frac{(m_1 + m_2)v^2}{2} = \frac{m_1 m_2}{2(m_1+m_2)}(v_1-v_2)^2
\end{equation}