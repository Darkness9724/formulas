\section{Механика орбит}

Правило Тициуса-Боде --- эмпирическая формула для нахождения большой полуоси \(n\) планеты:
\begin{equation}
    a \approx a_0 + 2^k b
\end{equation}
\(a_0\) --- большая полуось первой планеты;
\(b\) --- планетная поправка;
\(k\) --- порядковый номер планеты (\(n-2\)).

Эмпирическая формула вычисления планетной поправки:
\begin{equation}
    b \approx 0.3 \sqrt[4]{\qty(\frac{M}{M_{\odot}})^{\pi}}
\end{equation}

\begin{xtable}{lccc}
    [Пример правила Тициуса--Боде для Солнечной системы]
    \toprule
    \thead{Планета} & \thead{\(k\)} & \thead{\(a\)} & \thead{В реальности} \\
    \midrule
    Меркурий & \(-\infty\) & 0.4 & 0.39 \\
    Венера & 0 & 0.7 & 0.72 \\
    Земля & 1 & 1 & 1 \\
    Марс & 2 & 1.6 & 1.52 \\
    Церера и П.А. & 3 & 2.8 & 2.77 \\
    Юпитер & 4 & 5.2 & 5.2 \\
    Сатурн & 5 & 10 & 9.54 \\
    Уран & 6 & 19.6 & 19.2 \\
    Нептун & — & Выпадает & 30.1 \\
    Плутон & 7 & 38.8 & 39.44 \\
    \bottomrule
\end{xtable}

Максимальный и минимальный допустимые радиусы стабильной орбиты соседней планеты или астероида, расположенного ближе к звезде, чем данная:
\begin{align}\label{eq:orbit_max}
    &r_{\max} = a \qty[(1-\epsilon) - n_{int}\sqrt[3]{\frac{m}{3M}}]
\end{align}
\begin{align}\label{eq:orbit_min}
    &r_{\min} = a \qty[(1+\epsilon) + n_{ext}\sqrt[3]{\frac{m}{3M}}]
\end{align}
\(\epsilon\) --- эксцентриситет орбиты данной планеты;
\(m\) --- масса данной планеты;
\(M\) --- масса звезды;
\(n_{int}\) и \(n_{ext}\) --- коэффициенты, зависящие от эксцентриситета.

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \caption{Схематическое изображение формул~\ref{eq:orbit_max} и~\ref{eq:orbit_min}}
        \includegraphics[scale=0.3]{orbit1}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{График для нахождения \(n_{int}\) и \(n_{ext}\)}
        \includegraphics[scale=0.24]{orbit2}
    \end{subfigure}
\end{figure}

Внутренний и внешний радиусы обитаемой зоны~\cite{jones}:
\begin{align}
    &r_{\min}=\sqrt{\frac{L/L_{\odot}}{(4.190\cdot 10^{-8} T^2 -2.139\cdot 10^{-4} T + 1.296) T }}
    &\\
    &r_{\max}=\sqrt{\frac{L/L_{\odot}}{(6.190\cdot 10^{-9} T^2 - 1.139\cdot 10^{-5} T + 0.2341) T }}
\end{align}

Радиус эффективной орбиты, когда планета получает столько же энергии, сколько и Земля:
\begin{equation}
    r_{э} = \sqrt{\frac{L}{L_{\odot}}}
\end{equation}

Радиус идеальной для развития жизни орбиты:
\begin{equation}
    r_{и} = 1.16 \sqrt{\qty(\frac{M}{M_{\odot}})^{\frac{n}{2}}}
\end{equation}

Третий закон Кеплера, вычисляющий сидерический период:
\begin{align}
    \label{eq:kepler}
    &\frac{a^3}{P^2} = \frac{G (M+m)}{4\pi}
    &\\
    &\qty(\frac{P}{2\pi})^2 = \frac{a^3}{G {M+m}}
    &\\
    &P \approx \sqrt{\frac{a^3}{M+m}}
\end{align}

Расстояние от планеты до звезды в произвольной точке орбиты:
\begin{equation}
    r = a \frac{1 - \epsilon^2}{1 + \epsilon \cos{\theta}}
\end{equation}
\(\theta\) --- истинная аномалия для эллиптических орбит вычисляется по формуле:
\begin{equation}
    \theta = \frac{\qty(\frac{\vec{v} h}{\mu} - \frac{\vec{r}}{|\vec{r}|}) \vec{r}}{\qty(\qty|\frac{\vec{v} h}{\mu} - \frac{\vec{r}}{\vec{r}}|) |\vec{r}|}
\end{equation}
\(\vec{v}\) --- вектор орбитальной скорости;
\(\vec{r}\) --- радиус-вектор;
\(h\) --- удельный относительный момент импульса (эквивалентно \(\vec{r} \vec{v}\));
\(\mu\) --- гравитационный параметр~\ref{eq:gravparam}.

При \(\epsilon = 0\), истинная аномалия равна средней аномалии в момент времени \(t\), вычисляемой по формуле:
\begin{equation}
    M = n(t - \tau)
\end{equation}
\(\tau\) --- эпоха прохождения тела через перицентр;
\(n\) --- среднее движение:
\begin{equation}
    n = \frac{2\pi}{P}
\end{equation}

Следовательно, истинная аномалия связана со средней через ряд Фурье:
\begin{equation}
    \theta = M + \qty(2e - \frac{1}{4} e^3) \sin{M} + \frac{5}{4} e^2 \sin{2M} + \frac{13}{12}e^3 \sin{3M} + \order{e^4}
\end{equation}
\(e\) --- число Эйлера (\nameref{sec:const_list});
\(\order{e^4}\) --- нотация, обозначающая, что ряд ограничен сверху функцией \(e^4\).

Или через функцию Бесселя:
\begin{align}
    &\theta = M + 2 \Sum[s=1][\infty]{\frac{1}{s}} \qty{J_s(s e) + \Sum[p=1][\infty]{\beta[J_{s-p}(s e) + J_{s+p}(s e)]}} \sin{s M}
    &\\
    &\beta = \frac{1}{e} [1-\sqrt{(1-e^2)}]
\end{align}

Истинная аномалия для гиперболических орбит связана с расстоянием между телами:
\begin{align}
    r = \frac{a(1-\epsilon)}{1+\epsilon \cos{\theta}}
    \cos{\theta} = \frac{a(1-\epsilon) - r}{\epsilon r}
\end{align}
\(a\) --- большая полуось, вычисляемая для гиперболических орбит по формуле:
\begin{equation}
    a = \frac{1}{\frac{2}{r} - \frac{v^2}{\mu}}
\end{equation}

Расстояние до звезды в перицентре и апоцентре:
\begin{align}
    r_p = a(1-\epsilon)
    \qquad
    r_a = a(1+\epsilon)
\end{align}

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
	\includegraphics[scale=0.3]{eccentricity}
    \caption{Эллиптическая орбита}
\end{wrapfigure}

Эксцентриситет и большая полуось, выраженные через расстояния в перицентре и апоцентре:
\begin{align}
    \epsilon = \frac{r_a - r_p}{r_a + r_p}
    \qquad
    a = \frac{r_a + r_p}{2}
\end{align}

Орбитальная скорость в произвольной точке орбиты:
\begin{equation}
    v = \sqrt{G M(\frac{2}{r}-\frac{1}{a})}
\end{equation}

Орбитальная скорость в перицентре и апоцентре:
\begin{align}
    v_p = \sqrt{\frac{G M}{a} \frac{1+\epsilon}{1-\epsilon}}
    \qquad
    v_a = \sqrt{\frac{G M}{a} \frac{1-\epsilon}{1+\epsilon}}
\end{align}

Трансверсальная и радиальная компонента вектора скорости:
\begin{align}
    v_0 = \frac{v_p r_p}{r}
    \qquad
    v_r = \pm \sqrt{v^2 - v_0^2}
\end{align}
Знак плюса ставится при \(\theta < \pi\) (т.е когда планета удаляется от звезды), знак минуса при \(\theta > \pi\) (когда приближается).

Синодический период:
\begin{equation}
    \frac{1}{S} = \qty|\frac{1}{P_1} - \frac{1}{P_2}|
\end{equation}
\(P_1\) и \(P_2\) --- находятся по третьем закону Кеплера.

Время прохождения по орбите от перицентра до заданной точки:
\begin{equation}
    t = P \frac{E - \epsilon \sin{E}}{2\pi}
\end{equation}
E --- эксцентрическая аномалия:
\begin{equation}
    E = \arccos{\frac{1 - r/a}{\epsilon}} = \arccos{\frac{\epsilon + \cos{\theta}}{1+\epsilon \cos{\theta}}}
\end{equation}
\(r\) --- расстояние до звезды в заданной точке;
\(\theta\) --- истинная аномалия в заданной точке.

Сфера действия --- расстояние, когда гравитационные силы тела меньшей массы сильнее силы тела большей массы:
\begin{equation}
    r_{д} = a(1-\epsilon) \qty(\frac{m}{M})^{\frac{2}{5}}
\end{equation}
\(a\) --- расстояние между ними;
\(\epsilon\) --- эксцентриситет;
\(m\) --- тело с меньшей массой;
\(M\) --- тело с большей.

Сфера Хилла --- максимальное расстояние от планеты до спутника, когда гравитационные силы тела большей массы влияют на тело меньшей массы:
\begin{equation}
    r_{Х} = a(1-\epsilon) \sqrt[3]{\frac{m}{3M}}
\end{equation}
Орбита спутника может быть стабильной на расстоянии не более \(0.53 r_{Х}\) от планеты (при прямом вращении) или \(0.69 r_{Х}\) (при ретроградном).

Радиус геосинхронной орбиты (на которой спутник висит над одной и той же точкой планеты):
\begin{equation}
    r_{г} = \sqrt[3]{G(M+m) \qty(\frac{T}{2\pi})^2}
\end{equation}
\(M\) --- масса родителя;
\(m\) --- масса спутника;
\(T\) --- период обращения родителя.

\begin{wrapfigure}{l}{0.5\textwidth}
    \centering
    \tikzsetnextfilename{lagrange_points}
    \begin{tikzpicture}[scale=0.25]
        % Variables
        \pgfmathsetmacro{\muu}{0.005}
        \pgfmathsetmacro{\R}{10}
        
        % Coordinates
        \coordinate (L1) at ({\R*(1-(\muu/3)^(1/3))},0);
        \coordinate (L2) at ({\R*(1+(\muu/3)^(1/3))},0);
        \coordinate (L3) at ({-\R*(1+5/12*\muu)},0);
        \coordinate (L4) at ({\R*(1/2*(1-2*\muu))},{\R*sqrt(3)/2});
        \coordinate (L5) at ({\R*1/2*(1-2*\muu)},{-\R*sqrt(3)/2});
        \coordinate (C1) at ({-\muu*\R},0);
        \coordinate (C2) at ({(1-\muu)*\R},0);
        \coordinate (G) at (0,0);
        
        %C2 orbit
        \draw [thick,
        postaction={decorate,
           decoration={markings,mark=at position 0.125 with {\arrow{>};},
           mark=at position 0.375 with {\arrow{>};},
           mark=at position 0.625 with {\arrow{>};},
           mark=at position 0.875 with {\arrow{>};}}}
        ] (0,0) circle ({(1-\muu)*\R});
        
        % 2 equilateral triangles
        \draw [draw=red, dashed, every edge/.append style={draw=red, dashed}]
             (L4) -- node[sloped] {$\parallel$} (C1) 
             (L4) -- node[sloped] {$\parallel$} (C2) 
             (L5) -- node[sloped] {$\parallel$} (C1) 
             (L5) -- node[sloped] {$\parallel$} (C2);
        \draw [draw=white, dashed, every edge/.append style={draw=red, dashed}]
             (C1) -- node[sloped] {$\parallel$} (C2);
        
        % Degrees on each angle
        \draw ({-\muu*\R+(1-\muu)*\R/5},0) arc (0:-60:{(1-\muu)*\R/5})  node[midway, below right] {$60^\circ$}; %C1
        \draw ({(1-\muu)*\R-(1-\muu)*\R/5},0) arc (180:240:{(1-\muu)*\R/5}) node[midway, below left] {$60^\circ$}; %C2
        \draw ({\R*1/2*(1-2*\muu)-(1-\muu)*\R/5*1/2},{-\R*sqrt(3)/2+(1-\muu)*\R/5*sqrt(3)/2}) arc (120:60:{(1-\muu)*\R/5}) node[midway, above] {$60^\circ$}; %L5
        
        % Axes
        \draw [thick, dashed] ({-(\R*(1+(\muu/3)^(1/3))+1))},0)--({\R*(1+(\muu/3)^(1/3))+1},0); %x axis
        \draw [thick, dashed] (0,{-(\R*(1+(\muu/3)^(1/3))+1))})--(0,{\R*(1+(\muu/3)^(1/3))+1}); %y axis
        
        % Celestial bodies
        \draw [thick, fill=yellow] (C1) circle (1); %C1
        \draw [thick, fill=cyan] (C2) circle (0.5); %C2
        
        % Lagrangian points
        \node at (L1) {\color{gray}{\large $\bullet$}}; %L1
        \node[above left] at (L1)  {\large $L_1$}; %L1
        
        \node at (L2) {\color{gray}{\large $\bullet$}}; %L2
        \node[above right] at (L2) {\large $L_2$}; %L2
        
        \node at (L3) {\color{gray}{\large $\bullet$}}; %L3
        \node[above left] at (L3)  {\large $L_3$}; %L3
        
        \node at (L4) {\color{gray}{\large $\bullet$}}; %L4
        \node[above right] at (L4) {\large $L_4$}; %L4
        
        \node at (L5) {\color{gray}{\large $\bullet$}}; %L5
        \node[below right] at (L5) {\large $L_5$}; %L5
    \end{tikzpicture}
    \caption{Точки Лагранжа}
\end{wrapfigure}

Расстояние от L1 и L2 (точки Лагранжа) до спутника (эмпирическая формула для случаев, когда планета значительно массивнее спутника):
\begin{equation}
    r_{1,2} \approx r \sqrt[3]{\frac{(m/M)\ll 1}{3}}
\end{equation}

Расстояние от L3 до планеты (эмпирическая формула):
\begin{equation}
    r_3 \approx r \sqrt[3]{\frac{1+[(m/M)\ll 1]}{12}}
\end{equation}
\\\\
Точные уравнения для нахождения расстояний до L1, L2 и L3:
\begin{align}
    &\frac{m}{M} \qty(\frac{r_1}{a})^5+3\frac{m}{M} \qty(\frac{r_1}{a})^4+3\frac{m}{M} \qty(\frac{r_1}{a})^3 - \qty(\frac{r_1}{a})^2 + 2\qty(\frac{r_1}{a}) - 1 = 0
    &\qquad \text{(до L1)}
    &\\
    &\frac{m}{M} \qty(\frac{r_2}{a})^5+3\frac{m}{M} \qty(\frac{r_2}{a})^4+3\frac{m}{M} \qty(\frac{r_2}{a})^3 - \qty(\frac{r_2}{a})^2 - 2\qty(\frac{r_2}{a}) - 1 = 0
    &\qquad \text{(до L2)}
    &\\
    &\frac{m}{M} \qty(\frac{r_3}{a})^5+2\frac{m}{M} \qty(\frac{r_3}{a})^4+\frac{m}{M} \qty(\frac{r_3}{a})^3 - \qty(\frac{m}{M} + 1) \qty(\frac{r_3}{a})^2 - 2\qty(\frac{m}{M}) \frac{r_3}{a} - \qty(\frac{m}{M}) = 0
    &\qquad \text{(до L3)}
\end{align}

L4 и L5 будут являться вершинами равностороннего треугольника, основанием которого является расстояние от планеты до спутника:
\begin{equation}
    r_{4,5} = r
\end{equation}

В отличии от постоянно нестабильных L1, L2 и L3, тело в точках L4 и L5 способно находиться в относительно устойчивом состоянии при:
\begin{equation}
    \frac{M}{m} > 24.96
\end{equation}
Это, например, практически любая система звезда-планета или планета-спутник (системы Солнце-Юпитер, Земля-Луна и Плутон-Харон исключение).

Орбитальный резонанс --- взаимосвязь орбитальных периодов двух небесных тел, соотносящихся как пара натуральных чисел.
\begin{xtable}{lclc}
    [Орбитальные резонансы для Солнечной системы]
    \toprule
    \thead{Планета} & \thead{Резонанс} & \thead{Время сближения} & \thead{Вероятность несовпадения}\\
    \midrule
    Меркурий-Венера & 9:23 & 200 лет & 0.19 \\
    Венера-Земля & 8:13 & 1000 лет & 0.065 \\
    Венера-Марс & 1:3 & 20 лет & 0.11 \\
    Земля-Марс & 1:2 & 8 лет & 0.24 \\
    Земля-Юпитер & 1:12 & 40 лет & 0.28 \\
    Юпитер-Сатурн & 2:5 & 800 лет & 0.13 \\
    Юпитер-Уран & 1:7 & 500 лет & 0.18 \\
    Сатурн-Уран & 7:20 & 20000 лет & 0.2 \\
    Сатурн-Нептун & 5:28 & 80000 лет & 0.052 \\
    Уран-Нептун & 1:2 & 2000 лет & 0.078 \\
    Нептун-Плутон & 2:3 & 20000 лет & 0.02475 \\
    \bottomrule
\end{xtable}

Потеря энергии и импульса через излучение гравитационных волн:
\begin{align}
    &-\left\langle\frac{\dd{E}}{\dd{t}}\right\rangle = \frac{32 G^4 m_1^2 m_2^2 (m_1 + m_2)}{5 c^5 a^5 (1-e^2)^{\frac{7}{2}}}\qty(1+\frac{73}{24}e^2 + \frac{37}{96}e^4)
    &\\
    &-\left\langle\frac{\dd{L_z}}{\dd{t}}\right\rangle = \frac{32 G^{\frac{7}{2}} m_1^2 m_2^2 \sqrt{m_1 + m_2}}{5 c^5 a^{\frac{7}{2}} (1-e^2)^2}\qty(1+\frac{7}{8}e^2)
\end{align}
\(e\) --- эксцентриситет;
\(a\) --- большая полуось;
\(m_1\) и \(m_2\) --- соответственно массы взаимодействующих тел.