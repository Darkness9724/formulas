\section{Оптика}
        
Показатель преломления света:
\begin{equation}
    \lambda = \frac{c}{v}
\end{equation}

Дисперсия света:
\begin{equation}
    n = a+\frac{b}{\lambda^2} + \frac{c}{\lambda}
\end{equation}
\(a\), \(b\) и \(c\) --- показатели, зависящие от материала.

Уравнение переноса излучения:
\begin{equation}
    \frac{\dd{T}}{\dd{r}} = -\frac{3k_T \rho}{16\pi  S c r^2  T^3}  L
\end{equation}
\(S\) --- площадь;
\(c\) --- скорость света;
\(L\) --- светимость;
\(k_T\) --- коэффициент поглощения Томсона:
\begin{equation}
    k_T = \frac{\sigma_T}{m_p}\qty(\frac{X+1}{2})
\end{equation}
\(X\) --- молярная доля водорода.

При отсутствии рассеивания, свет давит на поверхность с силой:
\begin{equation}
    p = \frac{I}{c}(1-T+R) \cos^2{\theta}
\end{equation}
\(I\) --- интенсивность падающего излучения;
\(T\) --- коэффициент пропускания;
\(R\) --- коэффициент отражения;
\(\theta\) --- угол падения.

Если свет рассеивается:
\begin{equation}
    p = \frac{I}{c}\qty[1+\frac{2}{3}(A-T_d)]
\end{equation}
\(A\) --- альбедо, \(T_d\) --- коэффициент диффузного пропускания~\eqref{eq:dif}.

Уравнение Эйнштейна для внешнего фотоэффекта:
\begin{align}
    h\nu = A + \frac{mv_{\max}^2}{2}
    \qquad eU_0 = h(\nu - \nu_0)
\end{align}
\(\nu\) --- частота падающего фотона;
\(A\) --- работа выхода электрона из металла;
\(U_0\) --- задерживающие напряжение;
\(\nu_0\) --- минимальная длина волны излучения красной границы фотоэффекта:
\begin{align}
    \nu_0 = \frac{A}{h}
\end{align}

Максимальная длина волны излучения красной границы фотоэффекта:
\begin{equation}
    \lambda_0 = \frac{hc}{A}
\end{equation}

Комптоновский сдвиг (изменение длины волны излучения при комптоновском рассеивании):
\begin{equation}
    \Delta \lambda = \lambda' - \lambda = \frac{h}{mc}(1-\cos{\theta}) = \frac{2h}{mc}\sin^2{\frac{\theta}{2}}
\end{equation}
\(\lambda\) и \(\lambda'\) --- длина волны падающего и рассеивающего излучения;
\(m\) --- масса электрона;
\(\theta\) --- угол рассеивания.

Коэффициент отражения и пропускания для s-поляризации:
\begin{align}
    R_s = \frac{\sin^2{\alpha - \beta}}{\sin^2{\alpha + \beta}}
    &\qquad T_s = 1 - R_s
\end{align}

Коэффициент отражения и пропускания для p-поляризации:
\begin{align}
    R_p = \frac{\tan^2{\alpha - \beta}}{\tan^2{\alpha + \beta}}
    &\qquad T_p = \frac{\sin^2{\alpha} \sin^2{\beta}}{\sin^2(\alpha + \beta) \cos^2{\alpha - \beta}}
\end{align}

Коэффициент отражения и пропускания для нормального падения:
\begin{align}
    R_s = \abs{\frac{n_2 - n_1}{n_2 + n_1}}^2
    &\qquad T_s = \frac{4n_1  n_2}{(n_2 + n_1)^2}
\end{align}
\(n_1\) и \(n_2\) --- коэффициенты, зависящие от угла падения \(\theta_i\).

\begin{figure}
    \caption{Формулы Френеля для нахождения \(R_p\) и \(R_s\)}
    \centering
    \includesvg{fresnel}
\end{figure}

В отсутствие поглощения и отражений выполняется соотношение: 
\begin{equation}\label{eq:dif}
    T = T_r + T_d
\end{equation}
\(T_r\) --- коэффициент направленного пропускания.

Закон Снеллиуса:
\begin{equation}
    \frac{\sin{\theta_2}}{\sin{\theta_1}} = \frac{v_2}{v_1} = \frac{n_2}{n_1}
\end{equation}

Фокусное расстояние сферического зеркала:
\begin{equation}
    f = \frac{R}{2}
\end{equation}
\(R\) --- радиус зеркала.

Высота изображения в сферическом зеркале:
\begin{equation}
    y' = y \frac{v}{u}
\end{equation}
\(y\) --- высота предмета, \(v\) --- расстояние от изображения до зеркала, \(u\) --- расстояние от зеркала до предмета.

При \(u>f\) формула вогнутого зеркала имеет вид:
\begin{equation}
    \frac{1}{u}+\frac{1}{v} = \frac{2}{R}
\end{equation}

При \(u<f\):
\begin{equation}
    \frac{1}{u}-\frac{1}{v} = \frac{2}{R}
\end{equation}

Следовательно:
\begin{equation}
    \frac{1}{u}+\frac{1}{v} = \frac{1}{f}
\end{equation}

Такое зеркало будет иметь увеличение:
\begin{equation}
    Г = \frac{f+v}{f} = \frac{v}{f}+1
\end{equation}

Угловое увеличение:
\begin{equation}
    W = \frac{\tan{\theta_2}}{\tan{\theta_1}}
\end{equation}

Формула выпуклого зеркала:
\begin{equation}
    \frac{1}{u}-\frac{1}{v} = -\frac{2}{R}
\end{equation}

Апертура объектива есть диаметр зеркала: 
\begin{equation}
    D = R^2
\end{equation}

Проницающая сила телескопа:
\begin{align}
    &m = 2.5\log_{10}\qty(\frac{D}{\beta}) \sqrt{\frac{K t}{s}}
    &\\
    &m = 5.5 + 2.5\log_{10}{D} + 2.5\log_{10}{Г}
\end{align}
\(\beta\) --- диаметр центрального максимума дифракционного изображения звезды (диск Эри) в секундах дуги;
\(K\) --- квантовый выход оптической системы телескопа, равный отношению числа зарегистрированных фотонов к числу попавших на приёмник излучения;
\(s\) --- яркость фона ночного неба;
\(t\) --- время экспозиции.

Угловое разрешение телескопа:
\begin{equation}
    \delta = \frac{\lambda}{D}
\end{equation}

Продольный эффект Доплера:
\begin{equation}
    \lambda = \lambda_0 \qty(1+\frac{v}{c})
\end{equation}
\(\lambda_0\) --- длина волны, излученной источником.

Полуширина колокола функции Гаусса:
\begin{equation}
    G = A \exp\qty[-\frac{(E-E_0)^2}{2\sigma^2}]
    w = [2\sqrt(2\ln{2})]\sigma
\end{equation}

Полуширина колокола функции Лоренца:
\begin{equation}
    L(v) = A\frac{\gamma}{(v-v_0)^2 + \gamma^2}
    w = 2\gamma
\end{equation}